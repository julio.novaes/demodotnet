using Microsoft.AspNetCore.Mvc;

namespace DemoDotNet.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class DemoController : ControllerBase
    {
      
        private readonly ILogger<DemoController> _logger;

        public DemoController(ILogger<DemoController> logger)
        {
            _logger = logger;
        }

        [HttpGet(Name = "info")]
        public String Get()
        {
            _logger.LogInformation("<<<<<<Get-Demo>>>>>");
            return "Online"; 
        }
    }
}